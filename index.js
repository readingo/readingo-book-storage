const express = require('express');
const fs = require('fs');
const path = require('path');
const bodyParser = require('body-parser');

const app = express();

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.get('/book/:id', (req, res, next) => {
	fs.readFile(__dirname + '/books/' + req.params.id + '.pdf', (err, data) => {
		if (!err) {
			return res.end(data);
		} else {
			return next(err);
		}
	})
});

app.post('/book/:id', (req, res, next) => {
	fs.writeFile(__dirname + '/books/' + req.params.id + '.pdf', atob(req.body.file), err => {
		if (!err) {
			return res.send('ok');
		} else {
			return next(err);
		}
	});
});

app.get('/thumbnail/:id', (req, res, next) => {
	fs.readFile(__dirname + '/thumbnail/' + req.params.id + '.png', (err, data) => {
		if (!err) {
			return res.end(data);
		} else {
			return next(err);
		}
	})
});

app.post('/thumbnail/:id', (req, res, next) => {
	fs.writeFile(__dirname + '/thumbnail/' + req.params.id + '.png', atob(req.body.file), err => {
		if (!err) {
			return res.send('ok');
		} else {
			return next(err);
		}
	});
});

app.listen(9005, () => console.log('Listening on port 9005'));
