FROM node:12-alpine
WORKDIR /app
COPY . .
EXPOSE 9005
CMD [ "node", "index.js" ]
